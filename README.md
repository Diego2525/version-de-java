# Debian and Ubuntu #

```
    sudo apt-get install ./jdk-11.0.3_linux-x64_bin.deb
    sudo update-alternatives --install /usr/bin/java java /usr/lib/jvm/jdk-11.0.3/bin/java 1
    sudo update-alternatives --install /usr/bin/javac javac /usr/lib/jvm/jdk-11.0.3/bin/javac 1
```